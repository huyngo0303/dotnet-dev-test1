﻿namespace SortArrayDESC;

public class Program
{
    static void Main(string[] args)
    {
        var sorted = SortArrayDesc(new int[] { 1 });
        Console.WriteLine(sorted);
    }

    public static int[] SortArrayDesc(int[] x)
    {
        Array.Sort(x);
        Array.Reverse(x);
        Console.WriteLine("\nDescending: ");
        for (int i = 0; i < x.Length; i++)
        {
            Console.Write(x[i] + " ");

        }
        return x;
        //kết quả hàm sau khi thực thi là mảng số nguyên mới đã sắp xếp theo thứ tự giảm dần
        throw new NotImplementedException();
    }

}